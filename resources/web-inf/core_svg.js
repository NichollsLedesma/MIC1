var current_index = 0;
var mapsvgdoc = null;

var mapa_corrientes = [
  {"localidad":"Itatí","x":415,"y":80},
  {"localidad":"San Cosme","x":250,"y":80},
  {"localidad":"Berón de Astrada","x":510,"y":100},
  {"localidad":"Ituzaingó","x":630,"y":250},
  {"localidad":"Capital","x":220,"y":120},
  {"localidad":"San Luis del Palmar","x":300,"y":130},
  {"localidad":"General Paz","x":450,"y":160},
  {"localidad":"San Miguel","x":550,"y":200},
  {"localidad":"Empedrado","x":210,"y":200},
  {"localidad":"Mburucuyá","x":350,"y":240},
  {"localidad":"Santo Tomé","x":820,"y":270},
  {"localidad":"Concepción","x":385,"y":350},
  {"localidad":"Saladas","x":270,"y":290},
  {"localidad":"Bella Vista","x":150,"y":360},
  {"localidad":"San Martín","x":625,"y":470},
  {"localidad":"San Roque","x":250,"y":420},
  {"localidad":"General Alvear","x":770,"y":440},
  {"localidad":"Mercedes","x":400,"y":500},
  {"localidad":"Lavalle","x":200,"y":470},
  {"localidad":"Curuzú cuatiá","x":315,"y":650},
  {"localidad":"Goya","x":125,"y":580},
  {"localidad":"Paso de los Libres","x":620,"y":610},
  {"localidad":"Esquina","x":100,"y":720},
  {"localidad":"Sauce","x":240,"y":710},
  {"localidad":"Monte Caseros","x":470,"y":770}
];

var localidades = shuffle(mapa_corrientes);

function onClickEvent(evt) {
  var elm = evt.target;
  console.log($(elm).attr('id'));
  checkAnswer($(elm));
}


function init_svg() {
  var elm = document.getElementById('corrientes');
  //console.log(elm);
  try {
    mapsvgdoc = elm.contentDocument;
  }
  catch(e) {
    mapsvgdoc = elm.getSVGDocument();
  }
  mapsvgdoc.documentElement.setAttributeNS(null, "focusHighlight", "none"); // prevents showing a focusrect in Opera 9.5
  mapsvgdoc.documentElement.addEventListener("mousedown", onClickEvent, false);
}



/*game*/

function showQuestion(){
    $('#question_title').html('<p class="center-align">Where is '+localidades[current_index].localidad+' ?</p>');
}
function finish(){
  var $toastContent = $('<p class="white-text mic_font"><img src="resources/images/tincho paying mini.png"/> Congratulations, you did it!!!</p>');
  Materialize.toast($toastContent, 5000, 'rounded');
}

function addText(){
  var newElement = document.createElementNS("http://www.w3.org/2000/svg", 'text'); //Create a path in SVG's namespace
  newElement.setAttribute("x",localidades[current_index].x);
  newElement.setAttribute("y",localidades[current_index].y);
  newElement.setAttribute("font-family","MIC");
  newElement.setAttribute("font-size","25px");
  newElement.setAttribute("fill","#EBB51C");
  newElement.textContent =localidades[current_index].localidad;
  mapsvgdoc.getElementById('text_here').appendChild(newElement);
}
function checkAnswer(val){
  var id = val.attr('id');
  if(current_index==localidades.length-1){
    val.attr('fill','#54C4C6');
    addText();
    current_index++;  
    console.log(current_index+'/'+localidades.length);
    finish();
  }else if(current_index < 25){
    if(id==localidades[current_index].localidad){
        val.attr('fill','#54C4C6');
        var $toastContent = '<p class="white-text mic_font"><img src="resources/images/tablero/star.png" width="35px"/> Very good!</p>';
        Materialize.toast($toastContent, 3000, 'rounded');
        addText();
        current_index++;  
        console.log(current_index+'/'+localidades.length);
        showQuestion();
    }else{
      if(id!='svg2' && id!=null && current_index<(localidades.length)){Materialize.toast("That's "+id+", let´s try it again!", 3000, 'rounded');}
    }
  }
}

function shuffle(array_) {
  console.log("shuffle");
  var array = array_.slice();
  var currentIndex = array.length, temporaryValue, randomIndex;
  // While there remain elements to shuffle...
  while (0 !== currentIndex) {
    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;
    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }
  return array;
}