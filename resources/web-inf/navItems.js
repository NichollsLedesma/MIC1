var item_selected;

$(function(){
	item_selected = $('#item0').closest('li');
});

function changeNavItem(item){
	item_selected.removeClass('active');
	item_selected = $(item).closest('li');
	item_selected.addClass('active');
	
	var content, title = $(item).text();
	console.log(title);
	$("#title").html(title);
	switch(title){
		case "Introduction":
				// content ='<h2><mic_a>New</mic_a> <mic_b>design</mic_b></h2><br><br><br><br><br>';
				content = "<p>MiC (Made in Corrientes) - English Book 1 cuenta la historia que se inicia con la llegada a Corrientes de Andy, un niño de Arizona, Estados Unidos, hijo de un explorador, Mr. Adams.</p>";
				content +="<p>Mr. Adams viaja a la Argentina para realizar trabajos de exploración científica en la provincia de Corrientes y lo hace acompañado de su hijo, Andy. Como ambos vivirán varios años en esta provincia, Mr. Adams decide enviar a Andy a una escuela de la Capital.</p>";
				content +="<p>El libro se inicia con la docente del curso presentando a Andy a sus compañeros. Andy habla un español muy básico. Sin embargo, ese no es un obstáculo, ya que pronto se adaptará a su nueva vida de la mano de sus nuevos amigos correntinos: Santi, María y Martín. Estos tres niños, junto a sus familiares y a un personaje muy especial, Tincho “el carpincho”, harán sentir a Andy Adams como en su casa y le enseñará las tradiciones de nuestra tierra.</p>";
				content +="<p>MiC English Book 1 se complementa con un Libro de Acompañamiento al Docente. Este recurso didáctico, íntegramente escrito en español, tiene como objetivo acompañar, guiar, orientar la ardua tarea de planificación que ustedes, maestros talleristas de inglés, desarrollan a diario.</p>";
				content +="<p>MiC English Book 1 fue creado en su totalidad en base al Diseño Curricular Jurisdiccional de Inglés para el Segundo Ciclo de la Escuela Primaria y refleja la concepción pedagógica de la enseñanza de una lengua extranjera de forma comunicativa.</p>";

			break;
		case "About MIC":
			// primer columna
			content = '<h3>Made in Corrientes - English Book 1</h3>';
			content += '<div id="left">';
			content += '<mic_a><h6>Desarrollo de texto y actividades</h6>';
			content += '<mic_b><ul><li>Lic. Clarisa Godoy</li><li>Prof. Clavia Analía Cordoba</li><li>Prof. María Claudia Del Grosso</li><li>Prof. Roxana Mariel Proz</li><li>Prof. Alicia Raquel Villalba</li><li>Prof. Etel Itatí Aguirre</li><li>Prof. Belkis María Aranda Ansermet</li><li>Prof. Fernando Matías Giménez</li><li>Prof. Iván Matías Zampedri</li><li>NSE. Prof. María Sofía Lorenzo</li><li>NSE. Edith Alejandra Lencina Malgor</li></ul></mic_b>';
			content += '<h6>Coordinación de proyecto</h6>';
			content += '<mic_b><ul><li>Lic. Clarisa Godoy</li><li>Prof. Clavia Analía Cordoba</li></ul></mic_b>';
			content += '<h6>Corrección</h6><mic_b><ul><li>Prof. Anabel de los Ángeles Córdoba</li></ul></mic_b>';
			content += '<h6>Lectura crítica y corrección</h6>';
			content += '<mic_b><ul><li>Lic. Clarisa Godoy</li><li>Prof. Clavia Analía Cordoba</li></ul></mic_b>';
			content += '<h6>Ilustraciones</h6><mic_b><ul><li>Luvio</li><li>luvio.com.ar</li></ul><p><b>ISBN: 978-987-42-0494-3</b></p></mic_b></div></mic_a>';
			
			// segunda columna
			content += '<div id="medio">';
			content += '<mic_a><h6>Diseño de Tapas</h6><mic_b><ul><li>María José Rodríguez Bertoni</li></ul></mic_b>';
			content += '<h6>Maquetación y diseño</h6><mic_b><ul><li>María José Rodríguez Bertoni</li></ul></mic_b>';
			content += '<h6>Coordinación editorial</h6><mic_b><ul><li>Belén Rodríguez Bertoni</li></ul></mic_b>';			
			content += '<h6>Dirección de Sistemas:</h6><mic_b>Encina, Carlos Ruben</mic_b>';
			content += '<h6>Desarrolladores:</h6>';
			content += '<mic_b><ul><li>Ledesma, Nicolás Maximiliano</li><li>Lopez, Mariano Emanuel Alejandro</li><li>Meza, Alberto Agustín</li><li>Silva, Pablo Ariel</li></ul></mic_b>';
			content += '<mic_b><p>Primera edición: abril de 2016<br>Tinta al Tiempo<br>Libro de edición Argentina<br>Queda hecho el depósito <br>que dispone la ley 11.723</p></mic_b></div></mic_a>';

			// tercer columna
			content += '<div id="rigth">';
			content += '<mic_a><h5>Gobernador de la Provincia</h5><mic_b>Dr. Horacio Ricardo Colombi</mic_b>';
			content += '<h5>Ministra de Educación</h5><mic_b>Lic. Susana Mariel Benítez</mic_b>';
			content += '<h5>Coordinador de Gabinete</h5><mic_b>Dr. Julio Navías</mic_b>';
			content += '<h5>Secretaria de Gestión Educativa</h5><mic_b>Dra. Gabriela Albornoz</mic_b>';
			content += '<h5>Subsecretario de Gestión administrativa</h5><mic_b>C P Mauro Andrés Rinaldi</mic_b>';
			content += '<h5>Secretario General</h5><mic_b>Juan Breard</mic_b>';
			content += '<h5>Dirección de Educación Intercultural Bilingüe</h5><mic_b>Lic. Clarisa Godoy</mic_b></div></mic_a>';
			
			break;
		case "Games":
			content = '<h2><mic_a>Choose<mic_b> a game</mic_b>...</mic_a></h2><br><br><br><br>';
			break;
		default:
			content = "Error";
			break;
	}
	$('#contenido').html(content);
	if($(item).attr("id")!='item1'){$('.button-collapse').sideNav('hide');}
	//console.log($('#nav-mobile')[0].style.transform);
}