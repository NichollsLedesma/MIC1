
var name_numbers, nro_solicitado, color_solicitado, color_selected="", colors = ["red","green","blue"];
var img_number = 10, string="";

function random(vector,tipo){ //  agregar que no se repita por eso el switch
	var aux_uno="";
	switch(tipo){ 
		case "number":
			nro_solicitado = filtrar_nro(vector);
			aux_uno = nro_solicitado;
		break;
		case "color":
			color_solicitado = vector[Math.floor(Math.random() * vector.length)];
			aux_uno = color_solicitado;
		break;
	}
	return aux_uno;
}
$("#button").on("click",function(){// function reset
	init();
});
function filtrar_nro(vector){
	var aux = vector[Math.floor(Math.random() * vector.length)];;
	var index = vector.indexOf(aux);
	if (index > -1) {
	    vector.splice(index, 1);
	}
	// for (var i = 0; i < vector.length; i++) {
	// 	console.log(vector[i]);
	// }
	// console.log("----------------------");
	
	return aux;

}
function build_questions(){
	string = '<div id="questions">';
	string = '<p>Color number <span class="resaltar">'+ random(name_numbers,"number") + '</span> with color <span class="resaltar">'+random(colors,"color")+'</span></p></div>';
	return string;
}
function build_img_numbers(){
	string='<div class="row" id="numbers">';
	for (var i = 0; i <= img_number; i++) {
		string += '<div class="col s1 number flow-text" onclick="pintar('+i+')" id="img'+i+'">'+i+'</div>';
		// string += '<div class="col s1 number flow-text tooltipped" onclick="pintar('+i+')" id="img'+i+'" data-delay="2500" data-tooltip="'+converterNroToLetra_hasta10(i)+'">'+i+'</div>';
	}
	string += '</div>';
	return string;
}
function init(){
	name_numbers = ["zero","one","two","three","four","five","six","seven","eight","nine","ten"];
	$( "#imagesnumbers" ).html(build_img_numbers());
	$( "#questions" ).html(build_questions());
}
$(document).ready(function(){ // my init
	init();
});

function pintar(nro_current){
	// Materialize.toast(converterNroToLetra_hasta10(nro_current), 1000);

	// console.log("color_selected: " + color_selected + " color_solicitado: " + color_solicitado);
	// console.log("nro_current: " + nro_current + " nro_solicitado: " + nro_solicitado);
	

	if (color_selected==color_solicitado && converterNroToLetra_hasta10(nro_current)==nro_solicitado) {
		Materialize.toast("Very Good!! :D", 2000);
		// do_paint(color_selected,nro_current);
		$("#img"+nro_current).css("color", color_selected);
		
		$(".color #text_"+color_selected).html('<div class="texto" id="text_'+color_selected+'"></div>');
		color_selected="";
		if (typeof name_numbers[0] !== 'undefined') {
			$( "#questions" ).html(build_questions()); // cambia la pregunta
		} else {
			$( "#questions" ).html('<div id="questions"></div>'); // cambia la pregunta
			alert("Done");
		}
		
	} else if(color_selected!=""){
		Materialize.toast("No good. Try again :(", 2000);
	} else {
		Materialize.toast("Select some color.", 2000);
	}
}
function select_color(color_current){ 
	// estaria copado que me cambie el cursor
	// $("#"+color_current).css('cursor', 'url(resources/images/images-numbers/pote_red.png)');
	if (color_selected!="") {
		$(".color #text_"+color_selected).html('<div class="texto" id="text_'+color_selected+'"></div>');	
	} 
	color_selected = color_current;
	$(".color #text_"+color_selected).html('<div class="texto" id="text_'+color_selected+'">'+color_selected+'</div>');
	
	
}

function converterNroToLetra_hasta10(nro){
	var retorno = "";
	switch (nro){
		case 0:
			retorno = "zero";
			break;
		case 1:
			retorno = "one";
			break;
		case 2:
			retorno = "two";
			break;
		case 3:
			retorno = "three";
			break;
		case 4:
			retorno = "four";
			break;
		case 5:
			retorno = "five";
			break;
		case 6:
			retorno = "six";
			break;
		case 7:
			retorno = "seven";
			break;
		case 8:
			retorno = "eight";
			break;
		case 9:
			retorno = "nine";
			break;
		case 10:
			retorno = "ten";
			break;
	}
	return retorno;
}
