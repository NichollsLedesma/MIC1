//$(document).ready(function () {
  //your code here
 // $(function () {
      
 //      $(".boxes").draggable(
 //          {revert: 'invalid',
 //           stop: function(){
 //            $(this).draggable('option','revert','invalid');
 //          }
 //         }
 //      );
      
 //      $(".droppable").droppable({
 //          drop: function (event, ui) {
 //            var draggable = ui.draggable;
 //            if(draggable.attr("value")==1)
 //                alert("correct!");
 //              else
 //                draggable.draggable('option','revert',true);
 //           }
 //      });
 //  });



//});
	$(document).ready(function () {
        $(initgame);
    });

	function initgame(){

	    var correctCards = 0;

	    var numeros = [0,1,2,3,4,5,6,7];
			//And then shuffle it:

		function shuffle(o) {
		    for(var j, x, i = o.length; i; j = parseInt(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
		    return o;
		};

		var random = shuffle(numeros);
		// console.log(random);


	    $(init);

	    function init() {

	        // Hide the success message
	        $("#success").hide();

	        // Reset the game
	        correctCards = 0;
	        $('#dragPile').html('');
	        $('#dragSlots').html('');

	        // Create the pile of shuffled cards
	        //1:vegetables, 2:fruits, 3: Daily products, 4:meat

	        var numbers =[];
	        numbers[0] = [1,2];
	        numbers[1] = [2,4];
	        numbers[2] = [3,1];
	        numbers[3] = [4,3];
	        numbers[4] = [5,4];
	        numbers[5] = [6,2];
	        numbers[6] = [7,1];
	        numbers[7] = [8,1];
	        //var numbers = [1, 2, 3, 4, 5, 6, 7, 8];
	        var sections = [2, 4, 1, 3, 4, 2, 1, 1];
	        var percent = [
		        '<img class="responsive-img" src="resources/images/food/apple.png">',
	            '<img class="responsive-img" src="resources/images/food/fish.png">',
	            '<img class="responsive-img" src="resources/images/food/carrot.png">',
	            '<img class="responsive-img" src="resources/images/food/milk.png">',
	            '<img class="responsive-img" src="resources/images/food/barbacue.png">',
	            '<img class="responsive-img" src="resources/images/food/banana.png">',
	            '<img class="responsive-img" src="resources/images/food/broccoli.png">',
	            '<img class="responsive-img" src="resources/images/food/salad.png">'
	            ];
	    //        


	        for (var i = 0; i < 8; i++) {
	        	// console.log('percent: '+percent[random[i]]+ ' numbers '+numbers[random[i]][0]+' sections: '+sections[random[i]]);

	            $('<div>' + percent[random[i]] + '</div>').data('number', numbers[random[i]][0]).attr('section', 'section' + sections[random[i]]).appendTo('#dragPile').draggable({
	                containment: '#content',
	                stack: '#dragPile div',
	                cursor: 'move',
	                revert: true
	            });
	        }

	        // Create the drag slots
	        var dsections = [1, 2, 3, 4];
	        var words = ['Drag here'];
	        for (var i = 0; i <= 3; i++) {
	            $('<div>' + words + '</div>').data('number', i).attr('section', 'section' + dsections[i]).appendTo('#dragSlots').droppable({
	                accept: '#dragPile div',
	                hoverClass: 'hovered',
	                drop: handleCardDrop
	            });
	        }

	    }

	    function handleCardDrop(event, ui) {
	        var slotNumber = $(this).attr('section');
	        var cardNumber = ui.draggable.attr('section');

	        // If the card was dropped to the correct slot,
	        // change the card colour, position it directly
	        // on top of the slot, and prevent it being dragged
	        // again

	        if (slotNumber == cardNumber) {
                
                var addTop=$( "div .correct[section='"+slotNumber+"']").length*70;
                
	            ui.draggable.addClass('correct');
	            ui.draggable.draggable('disable');

	            //order of the elements inside the drop zone
                if ((slotNumber === 'section1') || (slotNumber === 'section3')){
                	ui.draggable.position({
	                of: $(this),
	                my: 'left+'+140+' top+'+addTop,
	                at: 'left top'
	            });
                }else{
                ui.draggable.position({
	                of: $(this),
	                my: 'left top+'+addTop,
	                at: 'left top'
	            });	
                }
	            
	            ui.draggable.draggable('option', 'revert', false);
	      
	            correctCards++;
	        }

	        // If all the cards have been placed correctly then display a message
	        // and reset the cards for another go

	        if (correctCards == 8) {
	            //$("#success").fadeIn('slow');
	            Materialize.toast('Excellent', 4000);
	        }

	    }

	}