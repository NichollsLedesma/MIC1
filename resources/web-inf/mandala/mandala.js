$(document).ready(function () {
        
          var color = 'white';
          var selected = '';

          $(".selection").click(function () {
              color = $(this).css("background-color");
              $(".selected").html('Selected: ' + $(this).attr('name'));
          });

          // $(".selected").click(function(){
          //    $(this).css("background-color", color)
          // });

          $('path, ellipse').click(function () {
          var id = $(this).attr('id');
          var svgElement = document.getElementById(id);
          svgElement.style.fill = color;
        });
  });