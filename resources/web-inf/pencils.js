var route = "resources/images/lapices/";
var cant_filas = 2; var cant_columnas = 4; // para luego ponerlo aleatorio y mas dificultades
var question_available = ["How many BLUE pencils are there?","How many RED pencils are there?"];
var question_selected = question_available[Math.floor(Math.random() * question_available.length)];
var img_selected, img_random  = [route+"l_azul.png",route+"l_roja.png"];
var answer = ["zero","one","two","three","four","five","six","seven","eight"];
var fila = "";
var correct ,cont_blue = 0;



iniciar();

function iniciar(){
	$(function() {
		$("#lapices").html(show_pencils());
		$("#pregunta").html(question_selected);
	});
	to_ask(cont_blue); 
	quitar_lapices(4000);
}

function show_pencils(){
	for (var j = 0; j < cant_filas; j++) { // cantidad de filas
		fila += '<div class="row">';

		for (var inclinacion, i = 0; i < cant_columnas; i++) { // cantidad de columnas
			inclinacion = Math.floor(Math.random() * 100) + 1  ; // valor entre 1 y 100
			img_selected = img_random[Math.floor(Math.random() * img_random.length)];
			fila += '<div class="col s3 responsive-img" style="-ms-transform: rotate('+inclinacion+
				'deg);-webkit-transform: rotate('+inclinacion+'deg); transform: rotate('+inclinacion+'deg);"><img src="' 
				+ img_selected + '" alt="img"></div>';
				
			if (img_selected == img_random[0]) { // veo si es azul
				cont_blue++;
			}

		}
		fila += '</div>';	
	};
	return fila;
}


function reset(){
	fila=""; cont_blue = 0;
	question_selected  = question_available[Math.floor(Math.random() * question_available.length)];
	iniciar();

}

function mostrar_de_nuevo(){
	$(function() {
		$("#lapices").html(fila);
		quitar_lapices(3000);

	});
}

function quitar_lapices(time){
	$("#show").addClass("disabled");
	setTimeout(function(){ 
		$(function() {
			$("#lapices").html('');
			$("#show").removeAttr("disabled");
			$("#show").removeClass("disabled");
		});			
	}, time);

}

function esta_cargado(new_nro, vec){
	var control = false;
	for (var i = 0; i < vec.length; i++) {
		
		if (new_nro == vec[i]) { 
			control = true;
		}
	};
	return control;
}
function cargar_vector(cantidad_elementos,vector){
	for (var aux, control, i = 0; i < cantidad_elementos; i++) {
		control = true;
		while (control) {
			aux = answer[Math.floor(Math.random() * answer.length)];
			if (!esta_cargado(aux,vector)) {
				vector.push(aux);
				control = false;
			}
		}
	};
}

function to_ask(cont_azules){
	correct = answer_right(cont_azules);
	var options = [correct];
	cargar_vector(2,options);
	//for (var i = 0; i < options.length; i++) {console.log(i+": "+options[i]);};// mostrar lo que tiene el vector

	// hacemos que se pongan aleatorio
	options.sort(function(){return Math.random() -0.5});



	var ask ='<form action="#" id="form_radiob" name="form_radiob">';
	for (var i = 0; i < 3; i++) { //cantidad de rb
		ask += '<input name="rb_ask" type="radio" id="'+i+'"  value="'+options[i]+'" onclick="check_answer();"/><label class="rb" for="'+i+'" >'+options[i]+'</label>';
	};
	ask += '</form>';
	$("#ask").html(ask);

}

function check_answer(){
	//$("#reset").removeAttr("disabled") // una vez que pruebe una vez al menos va a poder resetear

	if (correct==$('#form_radiob input:radio[type=radio]:checked').val()) {
		$("#mensaje").html('<div class="retro" style="display: block;">Congratulations!! =D</div>');
		mostrar_de_nuevo();
		setTimeout(function(){
			$("#mensaje").html('<div id="mensaje"></div>');
			reset();
		}, 3000);
	} else {
		$("#mensaje").html('<div class="retro incorrecto" style="display: block">You are failed :( - Try again.</div>');
		setTimeout(function(){
			$("#mensaje").html('<div id="mensaje"></div>');
		}, 3000);
	}
	
}

function answer_right(nro){
	if (question_selected!=question_available[0]){ // si es rojo
		nro = (cant_filas*cant_columnas)-nro;	
	} 

	var retorno = "";
	switch (nro){
		case 0:
			retorno = "zero";
			break;
		case 1:
			retorno = "one";
			break;
		case 2:
			retorno = "two";
			break;
		case 3:
			retorno = "three";
			break;
		case 4:
			retorno = "four";
			break;
		case 5:
			retorno = "five";
			break;
		case 6:
			retorno = "six";
			break;
		case 7:
			retorno = "seven";
			break;
		case 8:
			retorno = "eight";
			break;
	}
	return retorno;
}