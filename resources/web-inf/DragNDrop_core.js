var current_score; 
var max_score;

function initLesson(div,json,inited_flag){
    //inicializar todo
	div.html(' ');current_score=0;max_score=0;
	//armar header + cardPanel
	var html_aux = '<div id="inner" class="mic_font"><div class="row"><div class="col s12"><h2>'+json['chapter']['title']+'</h2></div></div>\n'
		+cardPanel(json['message'],json['logo'])+'<h2 id="score" data-topscore=""></h2>'+'<div id="contentz" class="row">';
	//de las imagenes
	var path = json['path'];
    html_aux+='<div class="row">';
    var items_text = shuffle(json['items']);
    var items = shuffle(json['items']);
    var chapterID = json['chapter']['id'];
    for(i=0;i<Object.keys(items).length;i++){
        html_aux+='<div class="col .s1 .m2 .l4"><div id="drop'+items_text[i]['id']+chapterID+'" class="dropzone js-drop drop'+items_text[i]['id']+chapterID+'">'+items_text[i]['name']+'</div></div>';
        html_aux+='<div class="col .s1 .m2 .l4"><div id="drag'+items[i]['id']+chapterID+'" class="draggable js-drag drag'+items[i]['id']+chapterID+'" '+'data-name="'+items[i]['name']+'"><img class="responsive-img" width="75px" src="'+path+items[i]['img']+'"></div></div>';
        setMaxScore(i+1);
        //if para cargar una sola vez los eventos de drag & drop (drag,drop) "quedan guardados"--> dropzone que admite dragitem
        if (!inited_flag) {
            console.log('Init: '+'.drag'+items[i]['id']+chapterID+' => .drop'+items[i]['id']+chapterID);
            dragNdropByClass('.drag'+items[i]['id']+chapterID,'.drop'+items[i]['id']+chapterID);
            addOnHoldTap('.drag'+items[i]['id']+chapterID);
        }
    }

	div.html(html_aux+'</div>');
	//inicializar score
	refreshScore();
    //set intro image
    if (typeof json['intro_img'] !== 'undefined' && json['intro_img'] !='') {
        $("#intro_img").append('<div class="row"><div class="col s6 offset-s3"><img class="responsive-img" src="'+path+json['intro_img']+'"></div></div>');
    }
}
/*
function initLesson_withoutText(div,json,inited_flag){
    //inicializar todo
    div.html(' ');current_score=0;max_score=0;
    //de las imagenes
    var path = json['path'];
    //armar header + cardPanel
    var html_aux = '<div id="inner"><div class="col s2"><img src="'+json['chapter']['img']+'" alt="" class="responsive-img"></div>\n'
        +cardPanel(json['message'],json['logo'])+'<div id="intro_img"></div><div id="exe"><h2 id="score" data-topscore=""></h2>'+'<div id="contentz" class="row">';
    html_aux+='<div class="row">';
    var items = shuffle(json['items']);
    var aux_item_count=1;
    var chapterID = json['chapter']['id'];
    for(i=0;i<Object.keys(items).length;i++){
        if (typeof items[i]['to'] !== 'undefined') {
            //draggable
            html_aux+='<div class="col .s1 .m2 .l4"><div id="drag'+items[i]['to']+chapterID+'" class="draggable js-drag drag'+items[i]['id']+chapterID+'" '+'data-name="'+items[i]['name']+'"><img class="responsive-img" src="'+path+items[i]['img']+'"></div></div>';
            setMaxScore(aux_item_count++);
        }else{
            //dropzone
            html_aux+='<div class="col .s1 .m2 .l4"><div id="drop'+items[i]['id']+chapterID+'" class="js-drop drop'+items[i]['id']+chapterID+'"><img class="responsive-img" src="'+path+items[i]['img']+'"></div></div>';
        }
        //if para cargar una sola vez los eventos de drag & drop (drag,drop) "quedan guardados"--> dropzone que admite dragitem
        if (!inited_flag) {
            if (typeof items[i]['to'] !== 'undefined') {
                dragNdropByClass('.drag'+items[i]['id']+chapterID,'.drop'+items[i]['to']+chapterID);
                addOnHoldTap('.drag'+items[i]['id']+chapterID);
            }
        }

    }
    div.html(html_aux+'</div></div>');
    //inicializar score
    refreshScore();
    //set intro image
    if (typeof json['intro_img'] !== 'undefined' && json['intro_img'] !='') {
        $("#intro_img").append('<div class="row"><div class="col s6 offset-s3"><img class="responsive-img" src="'+path+json['intro_img']+'"></div></div>');
    }
}*/

function setMaxScore(max){max_score=max;}
//actualizar score
function refreshScore(){$("#score").html('<h2>Score: '+current_score+'/'+max_score+'</h2>');}

//al soltar (item a soltar, donde)
function onDROP(myself,target){
	$('#'+myself.id).hide();
    $('#'+target.id).hide();
    //incrementar y actualizar score
    current_score++;
    refreshScore();
    //verificar fin
	if(current_score == max_score){
		$("#contentz").html('<h1>Excellent!</h1><img class="responsive-img" src="resources/images/tincho paying.png">');
        $("#intro_img").html('');
		current_score = 0;
		max_score = 0;
	}
}

//burbuja
function shuffle(array_) {
  console.log("shuffle");
  var array = array_.slice();
  var currentIndex = array.length, temporaryValue, randomIndex;
  // While there remain elements to shuffle...
  while (0 !== currentIndex) {

    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
}
//crear panel
function cardPanel(message,img_path){
	return '<div class="col s12 mic_font">\n'
        +'<div class="card-panel grey lighten-5 z-depth-1">\n'
          +'<div class="row valign-wrapper">\n'
            +'<div class="col s2">\n'
              +'<img src="'+img_path+'" alt="" class="circle responsive-img">\n'
            +'</div>\n'
            +'<div class="col s10"><blockquote>\n'
              +'<span class="black-text">'+message+'</span>\n'
            +'</blockquote></div>\n'
          +'</div>\n'
        +'</div>\n'
      +'</div>';
}

function addOnHoldTap(item_class){
    interact(item_class)
  .on('hold', function (event) {
    /*event.currentTarget.classList.toggle('rotate');
    event.currentTarget.classList.remove('large');*/
    var item_name = $('#'+event.currentTarget.id).data('name');
    meSpeak.speak(item_name, { pitch: 75, speed: 120,variant: "f5"});
    //console.log(aux.val());
  });
}
/*drag&drop no tocar magia*/
function dragNdropByClass(drag,drop){
    var startPos = null;
    interact(drag)
        .draggable({
            onmove: function (event) {
                var target = event.target,
                    x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx,
                    y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy;

                target.style.webkitTransform =
                target.style.transform =
                    'translate(' + x + 'px, ' + y + 'px)';

                target.setAttribute('data-x', x);
                target.setAttribute('data-y', y);
            },
            onend: function (event) {
               var textEl = event.target.querySelector('p');
                
                textEl && (textEl.textContent =
                    'moved a distance of '
                    + (Math.sqrt(event.dx * event.dx +
                                 event.dy * event.dy)|0) + 'px');
            }
        })
        .inertia(true)
        .restrict({
            drag: "",
            endOnly: true,
            elementRect: { top: 0, left: 0, bottom: 1, right: 1 }
        })
        .snap({
            mode: 'anchor',
            anchors: [],
            range: Infinity,
            elementOrigin: { x: 0.5, y: 0.5 },
            endOnly: true
        })
        .on('dragstart', function (event) {
            if (!startPos) {
              var rect = interact.getElementRect(event.target);

              // record center point when starting the very first a drag
              startPos = {
                x: rect.left + rect.width  / 2,
                y: rect.top  + rect.height / 2
              }
            }

            // snap to the start position
            event.interactable.snap({ anchors: [startPos] });
        });


    interact(drop)
        // enable draggables to be dropped into this
        .dropzone({ overlap: 'center' })
        // only accept elements matching this CSS selector
        .accept(drag)
        // listen for drop related events
        .on('dragenter', function (event) {
            var dropRect = interact.getElementRect(event.target),
                dropCenter = {
                  x: dropRect.left + dropRect.width  / 2,
                  y: dropRect.top  + dropRect.height / 2
                };

            event.draggable.snap({
              anchors: [ dropCenter ]
            });

            var draggableElement = event.relatedTarget,
                dropzoneElement = event.target;

            // feedback the possibility of a drop
            dropzoneElement.classList.add('drop-target');
            draggableElement.classList.add('can-drop');
        })
        .on('dragleave', function (event) {
            event.draggable.snap(false);

            // when leaving a dropzone, snap to the start position
            event.draggable.snap({ anchors: [startPos] });

            // remove the drop feedback style
            event.target.classList.remove('drop-target');
            event.relatedTarget.classList.remove('can-drop');
        })
        .on('dropactivate', function (event) {
            // add active dropzone feedback
            event.target.classList.add('drop-active');
        })
        .on('dropdeactivate', function (event) {
            // remove active dropzone feedback
            event.target.classList.remove('drop-active');
            event.target.classList.remove('drop-target');
        })
        .on('drop', function (event) {
        	//console.log(event);
        	onDROP(event.relatedTarget,event.target);
        	/*$('#'+event.relatedTarget.id).hide();
        	$('#'+event.target.id).hide();*/
        });
}
