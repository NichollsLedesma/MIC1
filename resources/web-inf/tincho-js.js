 
// JavaScript will go here
 
 $( init );
 
function init() {
 
  // Hide the success message
  $('#successMessage').hide();
  $('#successMessage').css( {
    left: '580px',
    top: '250px',
    width: 0,
    height: 0
  } );
 
  // Reset the game
  correctCards = 0;
  $('#cardPile').html( '' );
  $('#cardSlots').html( '' );
 
  // Create the pile of shuffled cards
  let sentense=[];
  sentense[0]=["what","is","your","name","?"];
  sentense[1]=["where","are","you","from","?"];
  sentense[2]=["how","old","are","you","?"];
  sentense[3]=["what", "is", "your", "favorite", "colour?"];
  // sentense[4]=["sarasa1", "sarasa2", "sarasa3", "sarasa4", "sarasa?"];
  var numbers = [ 1, 2, 3, 4, 5 ];

  //generan un num aleatorio para saber que preguntatomar
  function getRandomInt(min, max) {
      return Math.floor(Math.random() * (max - min + 1)) + min;
  }
  let elegida =getRandomInt(0,3);
  // console.log(elegida);
  // console.log(sentense[elegida].length);
  //ordena las palabras
  numbers.sort( function() { return Math.random() - .5 } );

  for ( var i=0; i<sentense[elegida].length; i++ ) {
    $('<div class="col s2">' + sentense[elegida][numbers[i]-1] + '</div>').data( 'number', numbers[i] ).attr( 'id', 'card'+numbers[i] ).appendTo( '#cardPile' ).draggable( {
      containment: '#content',
      stack: '#cardPile div',
      cursor: 'move',
      revert: true
    } );
  }
 
  // Create the card slots
  var words = [ ' ', ' ', ' ', ' ', ' ' ];
  for ( var i=1; i<=5; i++ ) {
    $('<div class="col s2">' + words[i-1] + '</div>').data( 'number', i ).appendTo( '#cardSlots' ).droppable( {
      accept: '#cardPile div',
      hoverClass: 'hovered',
      drop: handleCardDrop
    } );
  }
 
}


function handleCardDrop( event, ui ) {
  var slotNumber = $(this).data( 'number' );
  var cardNumber = ui.draggable.data( 'number' );
 
  // If the card was dropped to the correct slot,
  // change the card colour, position it directly
  // on top of the slot, and prevent it being dragged
  // again
 
  if ( slotNumber == cardNumber ) {
    ui.draggable.addClass( 'correct' );
    ui.draggable.draggable( 'disable' );
    $(this).droppable( 'disable' );
    ui.draggable.position( { of: $(this), my: 'left top', at: 'left top' } );
    ui.draggable.draggable( 'option', 'revert', false );
    correctCards++;
  } 
   
  // If all the cards have been placed correctly then display a message
  // and reset the cards for another go
 
  if ( correctCards == 5 ) {
    $('#successMessage').show();
    $('#successMessage').animate( {
      left: '50%',
      top: '250px',
      width: '25rem',
      height: '13rem',
      opacity: 1
    },1500 );
  }
 
}