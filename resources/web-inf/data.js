var InTheClassroom_lesson2 = {
	"message":"Drag and drop the faces to the correct feel name.<br>*Tip: hold click to listen the audio",
	"logo":"resources/images/tincho reading.png",
	"path":"resources/images/InTheClassroom/lesson2/",
	"chapter":{"id":"InTheClassroom2","name":"In The Classroom","lesson":2,"title":"<mic_b>In The</mic_b> <mic_a>Classroom</mic_a>"},
	"items":[
		{"id":1,"name":"Happy","img":"happy.png"},
		{"id":2,"name":"Sad","img":"sad.png"},
		{"id":3,"name":"Nervous","img":"Nervous.png"},
		{"id":4,"name":"Shy","img":"shy.png"}
	]
};
var MyClassroom_lesson4 = {
	"message":"Drag and drop the objects to its correct name.<br>*Tip: hold click to listen the audio",
	"logo":"resources/images/tincho reading.png",
	"path":"resources/images/MyClassroom/lesson4/",
	"chapter":{"id":"MyClassroom4","name":"My Classroom","lesson":4,"title":"<mic_b>My</mic_b> <mic_a>Classroom</mic_a>"},
	"items":[
		{"id":1,"name":"Pencil","img":"pencil.png"},
		{"id":2,"name":"Pen","img":"pen.png"},
		{"id":3,"name":"Ruler","img":"ruler.png"},
		{"id":4,"name":"Schoolbag","img":"schoolbag.png"},
		//{"id":10,"name":"Chalk","img":"Chalk.png"},
		{"id":5,"name":"Book","img":"book.png"},
		{"id":6,"name":"Rubber","img":"rubber.png"},
		{"id":7,"name":"Desk","img":"desk.png"},
		{"id":8,"name":"Chair","img":"chair.png"},
		{"id":9,"name":"Pencil case","img":"pencil case.png"}
	]
};

var MyFamily_lesson2 = {
	"message":"Drag and drop to match the classroom objects to the picture of each family member.<br>*Tip: hold click to listen the audio",
	"logo":"resources/images/tincho reading.png",
	"path":"resources/images/MyFamily/lesson2/",
	//"intro_img":"story.png",
	"chapter":{"id":"MyFamily2","name":"My Family","lesson":2,"title":"<mic_b>My</mic_b> <mic_a>Family</mic_a>"},
	"items":[
		//{"id":1,"name":"Marker","img":"marker.png"},
		//{"id":2,"name":"Ruler","img":"ruler.png"},
		//{"id":3,"name":"Schoolbag","img":"schoolbag.png"},
		{"id":4,"name":"Mum","img":"mum.png"},
		{"id":5,"name":"Dad","img":"dad.png"},
		{"id":6,"name":"Tincho","img":"tincho.png"}
	]
}

var Yummy={
	"message":"Drag and drop the food to its correct name.<br>*Tip: hold click to listen the audio",
	"logo":"resources/images/tincho reading.png",
	"path":"resources/images/Yummy/",
	"chapter":{"id":"Yummy","name":"Yummy","lesson":2,"title":"<mic_b>Yummy!</mic_b>"},
	"items":[
		{"id":1,"name":"Apple","img":"apple.png"},
		{"id":2,"name":"Banana","img":"banana.png"},
		{"id":3,"name":"Barbecue","img":"barbecue.png"},
		{"id":4,"name":"Broccoli","img":"broccoli.png"},
		{"id":5,"name":"Burger","img":"burger.png"},
		{"id":6,"name":"Carrot","img":"Carrot.png"},
		//{"id":7,"name":"Chiken","img":"chiken.png"},
		{"id":8,"name":"Fish","img":"fish.png"},
		{"id":9,"name":"Hot dog","img":"hotDog.png"},
		{"id":10,"name":"Ice cream","img":"iceCream.png"},
		//{"id":11,"name":"Chipá","img":"chipá.png"},
		//{"id":12,"name":"Chips","img":"chips.png"},
		{"id":14,"name":"Milk","img":"milk.png"},
		{"id":15,"name":"Pasta","img":"pasta.png"},
		{"id":16,"name":"Salad","img":"salad.png"},
		{"id":17,"name":"Soup","img":"soup.png"}
	]	
}

var AtTheClub={
	"message":"Drag and drop the sports to its correct name.<br>*Tip: hold click to listen the audio",
	"logo":"resources/images/tincho reading.png",
	"path":"resources/images/AtTheClub/",
	"chapter":{"id":"AtTheClub","name":"AtTheClub","lesson":2,"title":"<mic_b>At The </mic_b><mic_a>Club</mic_a>"},
	"items":[
		{"id":1,"name":"Basketball","img":"basketball.png"},
		{"id":2,"name":"Football","img":"football.png"},
		{"id":3,"name":"Gymnastic","img":"Gymnastic.png"},
		{"id":4,"name":"Rugby","img":"rugby.png"},
		{"id":5,"name":"Running","img":"running.png"},
		{"id":6,"name":"Skating","img":"skating.png"},
		{"id":7,"name":"Swimming","img":"swimming.png"},
		{"id":8,"name":"Taekwondo","img":"taekwondo.png"},
		{"id":9,"name":"Tennis","img":"tennis.png"},
		//{"id":10,"name":"Volleyball","img":"volleyball.png"},
		{"id":11,"name":"Yoga","img":"yoga.png"}
	]	
}

var InMyCity={
	"message":"Drag and drop the places to its correct name.<br>*Tip: hold click to listen the audio",
	"logo":"resources/images/tincho reading.png",
	"path":"resources/images/InMyCity/",
	"chapter":{"id":"InMyCity","name":"InMyCity","lesson":2,"title":"<mic_b>In My</mic_b><mic_a> City</mic_a>"},
	"items":[
		{"id":1,"name":"Bank","img":"bank.png"},
		{"id":2,"name":"Bridge","img":"bridge.png"},
		{"id":3,"name":"Church","img":"church.png"},
		{"id":4,"name":"Cinema","img":"cinema.png"},
		{"id":5,"name":"Fire station","img":"fireStation.png"},
		{"id":6,"name":"Gas station","img":"gasStation.png"},
		{"id":7,"name":"Hospital","img":"hospital.png"},
		{"id":8,"name":"Hotel","img":"hotel.png"},
		{"id":9,"name":"Pharmacy","img":"pharmacy.png"},
		{"id":10,"name":"Police station","img":"policeStation.png"}
		//{"id":11,"name":"Supermarket","img":"supermarket.png"}
	]	
}



/*var MyFamily_lesson2 = {
	"message":"Drag and drop to match the classroom objects to the picture of each family member.<br>*Tip: hold click to listen the audio",
	"logo":"resources/images/tincho reading.png",
	"path":"resources/images/MyFamily/lesson2/",
	"intro_img":"story.png",
	"chapter":{"id":"MyFamily2","name":"My Family","lesson":2,"img":"resources/images/my family.png"},
	"items":[
		{"id":1,"name":"Marker","img":"marker.png","to":5},
		{"id":2,"name":"Ruler","img":"ruler.png","to":4},
		{"id":3,"name":"Schoolbag","img":"schoolbag.png","to":6},
		{"id":4,"name":"Mum","img":"mum.png"},
		{"id":5,"name":"Dad","img":"dad.png"},
		{"id":6,"name":"Tincho","img":"tincho.png"}
	]
}*/