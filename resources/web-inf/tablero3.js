var tablero3 = {
	"lockers":[
		{"id":1,"questions":[
			{"question":"Where's the Manuel Belgrano bridge?","options":["Corrientes","Buenos Aires","Mendoza","Entre Rîos"],"answer":"Corrientes"}
		]},
		{"id":2,"questions":[
			{"question":"What place is this?","options":["Hospital","Park","Club","Police Station","Hotel"],"answer":"Hospital"}
		]},
		{"id":3,"questions":[
			{"question":"What color is the tray?","options":["Silver","Blue","Orange","Pink","Brown"],"answer":"Silver"}
		]},
		{"id":4,"questions":[
			{"question":"What color is the basketball?","options":["Orange","Red","Grey","Gold"],"answer":"Orange"}
		]},
		{"id":5,"questions":[
			{"question":"The father of who is visiting Corrientes?","options":["Andy","Martin","María","Santi","Tincho"],"answer":"Andy"}
		]},
		{"id":6,"questions":[
			{"question":"What place is this?","options":["Club","School","Supermarket"],"answer":"Supermarket"}
		]},
		{"id":7,"questions":[
			{"question":"What place is this?","options":["Police Station","Bank","Pharmacy","Gas Station"],"answer":"Police Station"}
		]},
		{"id":8,"questions":[
			{"question":"What place is this?","options":["Gas Station","Park","Fire Station","Cinema","Hospital"],"answer":"Gas Station"}
		]},
		{"id":9,"questions":[
			{"question":"How many big parks there is in Corrientes?","options":["One","Two","Four","Six"],"answer":"Four"}
		]},
		{"id":10,"questions":[
			{"question":"What place is this?","options":["Church","Shopping Mall","Park","Cinema","Zoo"],"answer":"Church"}
		]},
		{"id":11,"questions":[
			{"question":"What place is this?","options":["Cinema","Park","Bank","Fire Station"],"answer":"Cinema"}
		]},
		{"id":12,"questions":[
			{"question":"What place is this?","options":["Park","Pharmacy","Bank", "Zoo"],"answer":"Pharmacy"}
		]},
		{"id":13,"questions":[
			{"question":"There is a zoo in Corrientes?","options":["True","False"],"answer":"False"}
		]},
		{"id":14,"questions":[
			{"question":"What color is the mail box?","options":["Blue","Black","White","Red","Yellow"],"answer":"Blue"}
		]},
		{"id":15,"questions":[
			{"question":"What place is this?","options":["Gas Station","Police Station","Fire Station"],"answer":"Fire Station"}
		]},
		{"id":16,"questions":[
			{"question":"What color is the briefcase?","options":["Brown","Ligth Blue","Grey","Green"],"answer":"Brown"}
		]},
		{"id":17,"questions":[
			{"question":"What place is this?","options":["Bank","Park","Cinema","Church"],"answer":"Bank"}
		]},
		{"id":18,"questions":[
			{"question":"Where can you buy candies?","options":["Supermarket","Church","Zoo"],"answer":"Supermarket"}
		]},
		{"id":19,"questions":[
			{"question":"There is a shopping mall in Corrientes?","options":["True","False"],"answer":"True"}
		]}
	]
};