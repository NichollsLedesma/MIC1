var tablero2 = {
	"lockers":[
		{"id":1,"questions":[
			{"question":"From what sport is this ball?","options":["Basketball","Volleyball","Football","Rugby"],"answer":"Basketball"}
		]},
		{"id":2,"questions":[
			{"question":"Who are playing football?","options":["Andy","Martin","María","Santi","Tincho"],"answer":"Santi"}
		]},
		{"id":3,"questions":[
			{"question":"Who likes chipá?","options":["Andy","Martin","María","Santi","Tincho"],"answer":"Tincho"}
		]},
		{"id":4,"questions":[
			{"question":"The carrot is a ___?","options":["Vegetable","Fruit","Daily product","Meat"],"answer":"Vegetable"}
		]},
		{"id":5,"questions":[
			{"question":"Who are skating?","options":["Andy","Martin","María","Santi","Tincho"],"answer":"Andy"}
		]},
		{"id":6,"questions":[
			{"question":"What is this?","options":["Salad","Chips","Soup"],"answer":"Salad"}
		]},
		{"id":7,"questions":[
			{"question":"From what sport is this ball?","options":["Basketball","Volleyball","Football","Rugby"],"answer":"Rugby"}
		]},
		{"id":8,"questions":[
			{"question":"Who are playing tennis?","options":["Andy","Martin","María","Santi","Tincho"],"answer":"María"}
		]},
		{"id":9,"questions":[
			{"question":"The banana is a ___?","options":["Vegetable","Fruit","Daily product","Meat"],"answer":"Fruit"}
		]},
		{"id":10,"questions":[
			{"question":"Who can do yoga?","options":["Andy","Martin","María","Santi","Tincho"],"answer":"María"}
		]},
		{"id":11,"questions":[
			{"question":"The ice cream is a ___?","options":["Vegetable","Fruit","Daily product","Meat"],"answer":"Daily product"}
		]},
		{"id":12,"questions":[
			{"question":"Can Tincho run?","options":["Yes","No"],"answer":"Yes"}
		]},
		{"id":13,"questions":[
			{"question":"The burger is a ___?","options":["Vegetable","Fruit","Daily product","Meat"],"answer":"Daily product"}
		]},
		{"id":14,"questions":[
			{"question":"Who can do taekwondo?","options":["Andy","Martin","María","Santi","Tincho"],"answer":"Martin"}
		]},
		{"id":15,"questions":[
			{"question":"What sport is this?","options":["Volleyball","Tennis","Skating","Rugby"],"answer":"Volleyball"}
		]},
		{"id":16,"questions":[
			{"question":"What sport is this?","options":["Gymnastic","Taekwondo","Yoga","Rugby"],"answer":"Gymnastic"}
		]},
		{"id":17,"questions":[
			{"question":"The apple is a ___?","options":["Vegetable","Fruit","Daily product","Meat"],"answer":"Fruit"}
		]},
		{"id":18,"questions":[
			{"question":"Can Tincho swim?","options":["Yes","No"],"answer":"Yes"}
		]},
		{"id":19,"questions":[
			{"question":"The hot dog is a ___?","options":["Vegetable","Fruit","Daily product","Meat"],"answer":"Daily product"}
		]},
		{"id":20,"questions":[
			{"question":"Can Tincho ride a bike?","options":["Yes","No"],"answer":"No"}
		]}
	]
};