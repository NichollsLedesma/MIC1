var segundo_turno=false; // para controlar el exceso de click que le hagan despues de elegir los dos
var contador_intentos = 0;
var toastContent, aux, primer_turno = true;
var vectorImagen = new Array(20);
var v_done = new Array();


init();


function init(){
	$("#limpiar").addClass("disabled");
	cargar_vector();

}

function cargar_vector(){
	vectorImagen = [
		"basketball",
		"football",
		"Gymnastic",
		"rugby",
		"skating",
		"swimming",
		"taekwondo",
		"volleyball",
		"yoga",
		"tennis",
		"basketball",
		"football",
		"Gymnastic",
		"rugby",
		"skating",
		"swimming",
		"taekwondo",
		"volleyball",
		"yoga",
		"tennis"];

	// hacemos que se pongan aleatorio
	vectorImagen.sort(function(){return Math.random() -0.5});
}
 
function limpiar () {
	for (var i = 0; i < vectorImagen.length; i++) {
		document.getElementById(i+"a").src="resources/images/tincho reading.png";
	}
	contador_intentos = 0;
	// bandera_control = false;
	document.getElementById("contador").innerHTML='<p id="contador">Cantidad de intentos: ' + contador_intentos+'</p>';
	v_done = new Array();
	init();
} 

function done(id) {
	var ya_esta = false;
	for (var i = 0; i < v_done.length; i++) {
		if (v_done[i] == id) { 
			ya_esta = true;
			
		} 
	}
	return ya_esta;
}


function evento (img_id) {
	$("#limpiar").removeClass("disabled");

	if (!segundo_turno) {
		if (primer_turno) {// primer turno
			if (!done(img_id)) {
				primer_turno = false;
				aux = img_id;
				v_done.push(aux);
				$("#"+aux+"a").attr("src","resources/images/At the club/" + vectorImagen[aux] + ".PNG");	
			}
			
		} else {
			if (!done(img_id)) {
				primer_turno = true;
				segundo_turno=true;
				
				$("#"+img_id+"a").attr("src","resources/images/At the club/name_"+vectorImagen[img_id]+".PNG");
				
				if (vectorImagen[aux] == vectorImagen[img_id]) {
					v_done.push(img_id);
					
					toastContent = $('<p class="white-text mic_font"><img src="resources/images/tablero/star.png" width="35px"/> Very good!</p>');
					Materialize.toast(toastContent, 3000, 'rounded');

					// document.getElementById("resultado").innerHTML='<div class="retro" style="display: block;">Very good</div>';
					// setTimeout(function(){ 
					// 	document.getElementById("resultado").innerHTML='<div id="resultado"></div>';
					// }, 1000);

				} else {
					v_done.pop();
					// document.getElementById("resultado").innerHTML='<div class="retro incorrecto" style="display: block">Imagenes diferentes</div>'
					
					setTimeout(function(){ 
					   	
					   	$("#"+aux+"a").attr("src","resources/images/tincho reading.png");
					   	$("#"+img_id+"a").attr("src","resources/images/tincho reading.png");
						
					   	toastContent = $('<p class="white-text mic_font"> Let`s try it again </p>');
						Materialize.toast(toastContent, 2000, 'rounded');
						// document.getElementById("resultado").innerHTML='<div id="resultado"></div>';
					}, 1000);

				}

				// console.log(v_done);
				// simplemente para un pequeño delay
				setTimeout(function(){ 
					contador_intentos++;
				   	segundo_turno=false;
				   	// $("contador").value='<p id="contador">"Cantidad de intentos: " + contador_intentos</p>';
				   	document.getElementById("contador").innerHTML='<p id="contador">Cantidad de intentos: ' + contador_intentos+'</p>';
				}, 1000); 

			}

			
			
		}// fin del segundo turno

		setTimeout(function(){ 
			// FIN DE JUEGO
			if (v_done.length==vectorImagen.length) {
				// alert("Congratulations, great job!!");
				toastContent = $('<p class="white-text mic_font"><img src="resources/images/tincho paying mini.png"/> Congratulations, you did it!!!</p>');
  				Materialize.toast(toastContent, 5000, 'rounded');
			}
		}, 100); 
	}
	
	
	

}
