var tablero1 = {
	"lockers":[
		{"id":1,"questions":[
			{"question":"What classroom item is this?","options":["Pencil","Ruler","Pen"],"answer":"Pen"}
		]},
		{"id":2,"questions":[
			{"question":"Where is Andy from?","options":["Arizona, United States","Corrientes, Argentina"],"answer":"Arizona, United States"},
			{"question":"How old is Andy?","options":["Ten years old","Nine years old", "Eleven years old"],"answer":"Ten years old"},
			{"question":"How does Andy feel?","options":["Happy","Sad","Nervous","Shy"],"answer":"Happy"}
		]},
		{"id":3,"questions":[
			{"question":"How many pencils are there?","options":["Two","Too","Three"],"answer":"Two"}
		]},
		{"id":4,"questions":[
			{"question":"What number is this?","options":["One","Three","Tree"],"answer":"Three"}
		]},
		{"id":5,"questions":[
			{"question":"What color is this book?","options":["Red","Blue","Green"],"answer":"Blue"}
		]},
		{"id":6,"questions":[
			{"question":"What color is this School Bus?","options":["Yellow","Black","Red"],"answer":"Yellow"}
		]},
		{"id":7,"questions":[
			{"question":"What classroom item is this?","options":["Chair","Desk","Ruler"],"answer":"Chair"}
		]},
		{"id":8,"questions":[
			{"question":"What number is this?","options":["One","Five","Ten"],"answer":"One"}
		]},
		{"id":9,"questions":[
			{"question":"How does Martin feel?","options":["Happy","Shy","Sad","Nervous"],"answer":"Shy"}
		]},
		{"id":10,"questions":[
			{"question":"What classroom item is this?","options":["Pencil case","Schoolbag","Rubber"],"answer":"Schoolbag"}
		]},
		{"id":11,"questions":[
			{"question":"What classroom item is this?","options":["Desk","Ruler","Chair"],"answer":"Desk"}
		]},
		{"id":12,"questions":[
			{"question":"How does María feel?","options":["Happy","Shy","Sad","Nervous"],"answer":"Sad"}
		]},
		{"id":13,"questions":[
			{"question":"What classroom item is this?","options":["Pencil case","Rubber","Pen"],"answer":"Pencil case"}
		]},
		{"id":14,"questions":[
			{"question":"What number is this?","options":["Four","Six","Eight"],"answer":"Four"}
		]},
		{"id":15,"questions":[
			{"question":"How does Santi feel?","options":["Happy","Shy","Sad","Nervous"],"answer":"Nervous"}
		]},
		{"id":16,"questions":[
			{"question":"What classroom item is this?","options":["Rubber","Ruler","Pencil"],"answer":"Ruler"}
		]},
		{"id":17,"questions":[
			{"question":"What classroom item is this?","options":["Rubber","Pencil","Pen"],"answer":"Rubber"}
		]},
		{"id":18,"questions":[
			{"question":"What color is this house?","options":["Yellow","White","Orange"],"answer":"Yellow"}
		]}
	]
};